@extends("theme::layouts.master")
@section('page_title',__('Home'))
@section("content")
    @include("theme::components.home.banner")
    @include("theme::components.home.packages")
    @include("theme::components.home.features")
    @include("theme::components.home.faqs")
    @include("theme::components.home.services")

@endsection
@push("modals")
    <div id="modals-container"></div>
@endpush

