@extends("theme::layouts.master")
@section('page_title',__($page->page_title))
@section("content")
    <section class="single-about-us-section">
        <div class="container">
            <div class="single-about-us">
                {!!$page->content['text']??''!!}

            </div>
        </div>
    </section>
@endsection
