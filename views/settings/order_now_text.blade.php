@include('utilities::general.localization-head')
<div class="tab-content">
    <?php $i = 0 ?>
    @foreach (getAllowedLang() as $key => $locale)
        <div class="tab-pane {{  $i == 0 ? 'active' : '' }}" id="tab-{{$key}}">
            <div class="nav-tabs">
                <div class="form-group row">
                    <label class="control-label col-lg-12">@lang("Order now button text") </label>
                    <div class="col-lg-12">
                        <textarea class="form-control" name="order_now_button_{{$key}}_text"
                                  placeholder="{{ __("Text") }}">{{ old("order_now_button_{$key}_text",settings()->group("theme_car_services")->get("order_now_button_{$key}_text")) }}</textarea>
                        @error("order_now_button_{$key}_text")<p class="text-danger">{{ $message }}</p>@enderror
                    </div>
                </div>
            </div>
        </div>
            <?php $i++ ?>
    @endforeach
</div>
<hr>
