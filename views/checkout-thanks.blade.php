@extends("theme::layouts.master")
@section('page_title',__('Home'))
@section("content")
    @php($showContactUs = false)
    <section class="content-section status-content">
        <div class="container">
            <div class="status-flex">
                <div class="status-img loading-img text-center">
                    <img
                        class="lazy-img "
                        style=" filter: grayscale(1); opacity: 0.6; "
                        src="{{Ecommerce::theme()->asset('images/logo.png')}}"
                        alt="@lang('Logo')"
                    >
                </div>
                {{--                <img alt="logo" width="111" height="60"/>--}}
                <div class="text-center">
                    <h1 class="status mb-3 mt-2">

                        @lang('Order confirmed')
                    </h1>
                    <p>
                        @lang("We will contact you as soon as possible.")
                    </p>
                    <p>
                        @lang('Have a nice day')
                    </p>
                </div>

                <a href="{{ route('home') }}" class="cart-btn effects_">
                    @lang('Back to home')
                </a>
            </div>
        </div>
    </section>
@endsection
