<div class="breadcrumb-parant">
    <div class="container">
        <div class="breadcrumb_">
            <a href="{{route('home')}}" class="anc_gh">@lang("Home")</a>
            @foreach($breadcrumbs as $breadcrumb)
                <span>/</span>
                <h5 class="m_P_gh">{{$breadcrumb['title']}}</h5>
            @endforeach
        </div>
    </div>
</div>
