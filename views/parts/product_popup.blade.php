<form action="{{route('cart.add_only_item',$item->id)}}" method="POST"
      enctype="multipart/form-data"
      id="orderNow"
>
    @csrf
    <input type="hidden"
           name="quantity"
           value="{{Cart::getQuantityByModelId($item->id)?:1}}">

    <div class="order_popup">
        <div class="div_order_now">
            <button class="btn_gh btn_close_order" type="button">
                <i class="las la-times"></i>
            </button>
            <div class="order_img_">
                <img style="width: 263px; height: 263px" src="{{upload_storage_url($item->getImage())}}"
                     alt="{{$item->title}}"/>
            </div>
            <div class="content_order_now">
                <h4 class="m_P_gh title_order">{{$item->title}}</h4>
                <p class="m_P_gh paragraph_order">
                    {!! $item->description !!}
                </p>
                <span class="price_order"><span id="product_total_price"
                                                data-price="{{$item->pricing->finalPrice()}}">{{$item->pricing->finalPrice()}}</span> @lang('SAR')</span>
                <div>
                    @foreach($item->options as $option)
                        @includeWhen($option->type === "select","theme::components.shop.item_options_inputs._select")
                        @includeWhen($option->type === "checkbox","theme::components.shop.item_options_inputs._checkbox")
                        @includeWhen($option->type === "radio","theme::components.shop.item_options_inputs._radio")
                        @includeWhen($option->type === "textarea","theme::components.shop.item_options_inputs._textarea")
                        @includeWhen(in_array($option->type,['text','number','date']),"theme::components.shop.item_options_inputs._input")

                        @includeWhen($option->type == 'image' ,"theme::components.shop.item_options_inputs._image")
                        @includeWhen($option->type == 'file' ,"theme::components.shop.item_options_inputs._file")
                    @endforeach
                </div>
                <div class="Select-Size">
                    <a onclick="$('#orderNow').submit()"
                       class="btn_order_now effects_2 anc_gh">{{Ecommerce::OrderNowButtonText()}}</a>
                </div>
            </div>
        </div>
    </div>
</form>
