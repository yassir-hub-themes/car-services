<section class="contact_section" id="id-contact-us">
    <div class="title_div">
        <h4 class="m_P_gh">@lang("Get in touch")</h4>
        <h2 class="m_P_gh primary_color ">@lang('We welcome your inquiries')</h2>
    </div>
    <div class="container">
        <div class="contact">
            <div class="contact_right">
                @if($setting = settings("contact-address"))
                    <h3 class="m_P_gh">
                        <i class="las la-map-marker-alt"></i>
                        @lang("Address")
                    </h3>
                    <p class="address_">
                        {{$setting}}
                    </p>
                @endif
                <div class="contact_faq">
                    @if($setting = settings("contact-phone"))
                        <div class="group_contact">
                            <h3 class="m_P_gh">
                                <i class="las la-mobile"></i>
                                @lang("Phone")
                            </h3>
                            <a href="tel:{{$setting}}" class="anc_contact anc_gh">
                                {{$setting}}
                            </a>
                        </div>
                    @endif
                    @if($setting = settings("contact-main-email"))
                        <div class="group_contact">
                            <h3 class="m_P_gh">
                                <i class="las la-envelope"></i>
                                @lang("Email")
                            </h3>
                            <a href="mailto:{{$setting}}" class="anc_contact anc_gh">
                                {{$setting}}
                            </a>
                        </div>
                    @endif
                </div>
                <div class="social_media">
                    <ul class="ul_gh ul_social">
                        @foreach(array_filter(settings()->json("social_links"))??[] as $key => $social )
                            <li>
                                <a href="{{$social['link']}}" class="social-link text-decoration-none">
                                    <i class="{{$social['icon']}}"></i>
                                </a>
                            </li>
                        @endforeach
                        {{--                        <li>--}}
                        {{--                            <a href="#" class="anc_gh"--}}
                        {{--                            ><i class="lab la-instagram"></i--}}
                        {{--                                ></a>--}}
                        {{--                        </li>--}}
                        {{--                        <li>--}}
                        {{--                            <a href="#" class="anc_gh"><i class="lab la-telegram"></i></a>--}}
                        {{--                        </li>--}}
                        {{--                        <li>--}}
                        {{--                            <a href="#" class="anc_gh"><i class="lab la-twitter"></i></a>--}}
                        {{--                        </li>--}}
                        {{--                        <li>--}}
                        {{--                            <a href="#" class="anc_gh"--}}
                        {{--                            ><i class="lab la-facebook-f"></i--}}
                        {{--                                ></a>--}}
                        {{--                        </li>--}}
                    </ul>
                </div>
            </div>
            <div class="form_section">
                <p class="m_P_gh">
                    @lang('Send Us Your Inquiry Or Suggestion Now And We Will Respond To You As Soon As Possible')
                </p>
                <form action="{{ route('contact-us.store',['#id-contact-us']) }}" method="POST">
                    @csrf
                    <div class="group_input">
                        <div class="group_form">
                            <input
                                value="{{ old('name') }}"
                                name="name"
                                type="text"
                                class="form-control"
                                placeholder="@lang("Name")"
                            />
                            @error("name")
                            <span class="text-danger text-sm">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="group_form">
                            <input
                                type="phone" value="{{ old('phone') }}" name="phone"
                                placeholder="@lang('Phone')"
                                class="form-control"
                            />
                            @error("phone")
                            <span class="text-danger text-sm">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="group_input">
                        <div class="group_form">
                            <div class="position_r">
                                <i class="las la-angle-down angle_select"></i>
                                <select class="js-select-form-country" name="city_id" data-placeholder="@lang("Select City")">
                                    <option value=""></option>
                                    @foreach(\Tasawk\Locations\Models\City::get() as $city)
                                        <option @if($loop->first) selected @endif value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                                @error("city_id")
                                <span class="text-danger text-sm">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="group_form">
                            <input
                                class="form-control"
                                type="email" value="{{ old('email') }}" name="email"
                                placeholder="@lang('Email')"
                            />
                            @error("email")
                            <span class="text-danger text-sm">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="group_form">
                <textarea
                    class="form-control"
                    name="message"
                    placeholder="@lang('Message')"
                >{{ old('message') }}</textarea>
                        @error("message")
                        <span class="text-danger text-sm">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="btn_div">
                        <button class="btn_gh btn_send effects_2 form-submit">
                            @lang("Send")
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
