<div class="container">
    <div class="_code-inputs d-flex justify-content-center wid">
        <div class="code-input-cont" style="width: 50%">
            {{--            <label class="text-right">@lang("Discount coupon")</label>--}}
            @php($coupon = Cart::getConditionsByType("coupon")->first())
            <div class="code-input">
                <input class="discount-code-input @if($coupon) active @endif" type="text"
                       placeholder="@lang('Enter Code')"
                       value="{{optional($coupon)->getName()}}">
                <button class="code-btn-input">
                    @if($coupon)
                        <i class="fas fa-check"></i>
                    @endif
                </button>
            </div>
        </div>
        {{--        <div class="code-input-cont">--}}
        {{--            <label class="text-right">--}}
        {{--                @lang("Purchase coupons")--}}
        {{--            </label>--}}
        {{--            @php($voucher =Cart::getConditionsByType("voucher")->first())--}}

        {{--            <div class="code-input">--}}
        {{--                <input type="text" class=" voucher-code-input @if($voucher) active @endif"--}}
        {{--                       placeholder="@lang('Type your voucher code here')"--}}
        {{--                       value="{{optional($voucher)->getName()}}">--}}
        {{--                <button class="code-btn-input">--}}
        {{--                    @if($voucher)--}}
        {{--                        <i class="fas fa-check"></i>--}}
        {{--                    @endif--}}
        {{--                </button>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
</div>
