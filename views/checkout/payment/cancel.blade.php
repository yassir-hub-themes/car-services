@extends("theme::layouts.master")
@section('page_title',__('Payment canceled'))
@section("content")
    <section class="content-section status-content">
        <div class="container">
            <div class="status-flex">
                <div class="status-img loading-img">
                    <img class="lazy-img" data-src="{{Ecommerce::theme()->asset('images/delivery.png')}}">
                </div>
                <h1 class="status">
                    @lang('Sorry, Your payment has been canceled.')
                </h1>
                <a href="{{$history->data['invoice_url']}}">@lang('Pay Again')</a>
                <a href="{{ route('home') }}" class="cart-btn">
                    @lang('Back to home')
                </a>
            </div>
        </div>
    </section>
@endsection
