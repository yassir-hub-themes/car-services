@include("theme::layouts.header")
@includeWhen(isset($breadcrumbs),"theme::parts.breadcrumb")
@yield("content")
@include("theme::layouts.footer")

