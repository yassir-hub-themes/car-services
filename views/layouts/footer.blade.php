@php($showContactUs ??=true)
@includeWhen($showContactUs,"theme::parts.contact_us")
<footer>
    <div class="container">
        <p class="m_P_gh">@lang('All rights reserved for :BRAND © :DATE',['DATE' => date("Y"),'BRAND'=>trans("Baker")])</p>
    </div>
</footer>
<button class="btn_gh back_top show"></button>
<div class="whats-phone_fixed">

    <a href="tel:{{settings()->get('contact-phone')}}" class="anc_fixed phone_ anc_gh"><i class="las la-phone"></i></a>
    <a href="https://wa.me/{{settings()->get('contact-whatsapp')}}" class="anc_fixed whats_ anc_gh"><i
            class="lab la-whatsapp"></i></a>
</div>
@stack("modals")
<script src="{{Ecommerce::theme()->asset('js/jquery-3.4.1.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.umd.js"></script>
<script src="{{Ecommerce::theme()->asset('js/bootstrap.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/i18n/{{app()->getLocale()}}.js"></script>
<script src="{{Ecommerce::theme()->asset('js/popper.min.js')}}"></script>
<script src="{{Ecommerce::theme()->asset('js/script.js?v=1.0')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
@if(app()->getLocale() == 'ar')
    <script src="{{Ecommerce::theme()->asset('js/lang/ar.js')}}"></script>
@endif
<script>

        @if(session('success'))
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: "{{ session('success') }}",
            showConfirmButton: true,
            confirmButtonText: "@lang("OK")",
            timer: 3000
        })
        @endif
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': _token}
    });
    @if(session('error'))
    notify("@lang('Error')", "{{ session('error') }}", 'error');
    @endif
    function notify(title, message, type = 'success') {
        Swal.fire({
            position: 'center',
            icon: type,
            title: message,
            showConfirmButton: true,
            confirmButtonText: "@lang("OK")",
            timer: 3000
        })
    }
</script>
<script>
    function updateCartCount(count) {
        $(".cart_items_count").html(count);
    }

    function updateCartPopUp() {
        $.ajax({
            url: route("ajax.cart_popup"),
            success: function (data) {
                $(".cart-popup-container").html(data);
            }
        })
    }

    function cartUpdated(cart) {
        @if(Route::currentRouteName() == "checkout" )
        getCartTotalSummation()
        @endif
        updateCartCount(cart.quantity);
        updateCartPopUp()
        {{--notify("@lang('Done')", "@lang('Cart updated')");--}}
    }

    function addToCart(item, quantity) {
        $.ajax({
            url: route("add-to-cart", [item, quantity]),
            type: "POST",
            success: function (cart) {
                cartUpdated(cart);
            },
            error: function (dd) {
                location.assign(dd.responseJSON['redirect']);
            }
        })
    }

    function applyDeliveryServiceByArea(id, type = 'city') {
        $.ajax({
            url: route('cart.apply_delivery_service', [type, id]),
            type: "POST",
            beforeSend: function () {
                replaceContentWith("#cart-total-summation-container", $("#spinner").html());
            },
            success: function (data) {
                getCartTotalSummation();
            }
        })
    }

    function updateCartItemQuantity(cartId, quantity) {
        $.ajax({
            url: route("update-cart-item-quantity", [cartId, quantity]),
            type: "POST",
            success: function (cart) {
                replaceContentWith("#cart-total-summation-container", $("#spinner").html());
                cartUpdated(cart);
            },
            error: function (dd) {
                location.assign(dd.responseJSON['redirect']);
            }
        })
    }

    function getCartTotal() {
        $.ajax({
            url: route("cart.get_total"),
            success: function (data) {
                $(".cart_price").html(data);
            }
        })
    }

    function getCartTotalSummation() {
        $.ajax({
            url: route("cart.get-total-summation"),
            success: function (data) {
                $("#cart-total-summation-container").html(data);
            }
        })
    }

    function replaceContentWith(selector, text) {
        $(selector).children().remove();
        $(selector).html(text);
    }

    function changeIconState(selector, status = "loading") {
        let statusHtml = {
            loading: `<i class="fas fa-spinner"></i>`,
            success: `<i class="fas fa-check"></i>`,
            danger: `<i class="fas fa-times"></i>`,
            none: ""
        }
        $(selector).html(statusHtml[status]);
    }

    $(function () {
        // $('#button-cart').on("click", function () {
        //     let input = $(this).parents(".quantity_cart").find("#input-quantity");
        //     let quantity = input.val()
        //     let item = input.data("item");
        //     let cartId = input.data('cart-id');
        //     if (cartId) {
        //         updateCartItemQuantity(cartId, quantity)
        //     } else {
        //         addToCart(item, quantity);
        //     }
        // });

        $('.qty-control').not(".disabled-actions").on("click", function () {
            let input = $(this).parents(".item-qty").find("input.qty-input");
            let quantity = input.val()
            let item = input.data("item");
            let cartId = input.data('cart-id');
            if (!input.data('has-ajax')) {
                return;
            }
            if (quantity <= 0) {
                input.parents(".cart-item").remove();
            }
            if (cartId) {
                updateCartItemQuantity(cartId, quantity)
            } else {
                addToCart(item, quantity);
            }
        });
        $(".item-qty input.qty-input").not(".disabled-actions").on("change", function () {
            let input = $(this);
            let quantity = input.val()
            let item = input.data("item");
            let cartId = input.data('cart-id');
            if (quantity <= 0) {
                input.parents(".cart-item").remove();
            }
            if (cartId) {
                updateCartItemQuantity(cartId, quantity)
            } else {
                addToCart(item, quantity);
            }
        });
        $(document).on("click", "a.item-delete , .delete-item-cart-popup", function () {
            let that = $(this);
            let item = that.data("item");
            replaceContentWith("#cart-total-summation-container", $("#spinner").html());
            $.ajax({
                url: route("remove-item-from-cart", {item}),
                type: "DELETE",
                success: function (cart) {
                    @if(Route::currentRouteName() == "checkout" )
                    if (cart.length === 0) {
                        location.href = '/'
                        // replaceContentWith(".cart-content", $("#empty-cart").html());
                    }
                    getCartTotalSummation();
                    that.parents(".cart-item").remove();
                    @endif
                    if (that.hasClass('delete-item-cart-popup')) {
                        updateCartPopUp();
                    }
                    updateCartCount(cart.length);
                    notify("@lang('Success')", "@lang('Item removed from cart')");

                }
            })
        })
        $(".discount-code-input").on("change", function () {
            let that = $(this);
            let coupon = that.val();
            changeIconState(that.parent().find(".code-btn-input"));
            replaceContentWith("#cart-total-summation-container", $("#spinner").html());
            $.ajax({
                url: route("cart.apply-coupon", {coupon}),
                type: "POST",
                success: function (data) {
                    if (data.status === 200) {
                        if (coupon === "") {
                            changeIconState(that.parent().find(".code-btn-input"), "none");
                        } else {
                            changeIconState(that.parent().find(".code-btn-input"), "success");
                            that.parent().find("input").addClass('active');
                        }
                        replaceContentWith("#cart-total-summation-container", $("#spinner").html());
                        // getCartTotalSummation();
                        getCartTotal()
                        notify("@lang('Success')", data.message);

                    } else {
                        changeIconState(that.parent().find(".code-btn-input"), "danger");
                        that.parent().find("input").removeClass('active');
                        notify("@lang('Error')", data.message, "error");
                        getCartTotal()
                        // getCartTotalSummation();


                    }

                },
            })
        })
        $(".voucher-code-input").on("change", function () {
            let that = $(this);
            let coupon = that.val();
            changeIconState(that.parent().find(".code-btn-input"));
            $.ajax({
                url: route("cart.apply-voucher", {coupon}),
                type: "POST",
                success: function (data) {
                    if (data.status === 200) {
                        if (coupon === "") {
                            changeIconState(that.parent().find(".code-btn-input"), "none");
                        } else {
                            changeIconState(that.parent().find(".code-btn-input"), "success");
                            that.parent().find("input").addClass('active');
                        }
                        replaceContentWith("#cart-total-summation-container", $("#spinner").html());
                        getCartTotalSummation();
                        notify("@lang('Success')", data.message);
                    } else {
                        changeIconState(that.parent().find(".code-btn-input"), "danger");
                        notify("@lang('Error')", data.message, "error");

                    }

                },
            })

        });

        const options = [];

        function handleSelectedOptions(option_id, option_value, option_price) {
            if (options.find(option => option.option_id === option_id)) {
                let index = options.findIndex(option => option.option_id === option_id);
                options[index]["option_value"] = option_value;
                options[index]["option_price"] = option_price;
            } else {
                options.push({option_id, option_value, option_price})
            }
        }

        function recalculateProductPriceWithSelectedOptions() {
            let product_price = $("#product_total_price");
            let total = options.reduce((a, b) => a + parseFloat(b.option_price), 0) + parseFloat(product_price.data('price'));
            ;
            console.log(options, total);
            product_price.html(total);
        }

        $(document).on('change', '.selected-item-option', function () {
            let option_id = $(this).data('id');
            let option_price = $(this).find('option:selected').data('price') ?? 0;
            let option_value = $(this).find('option:selected').val();
            handleSelectedOptions(option_id, option_value, option_price);
            recalculateProductPriceWithSelectedOptions();
        });
        $(document).on('change', '.radio-item-option', function () {
            let option_id = $(this).data('id');
            let option_value = $(this).val();
            let option_price = $(this).data('price');
            handleSelectedOptions(option_id, option_value, option_price);
            recalculateProductPriceWithSelectedOptions();
        });
    });
</script>
@stack("scripts")
</body>
</html>
