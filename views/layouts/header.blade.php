<!DOCTYPE html>
<html lang="{{app()->getLocale()}}" dir="{{Utilities::isRtl()?'rtl':'ltr'}}">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet"
          href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css"/>
    <link rel="icon" href="{{Ecommerce::theme()->asset('images/logo.png')}}" type="image/png"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"/>
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset('style/bootstrap.min.css')}}"/>
    @if(Utilities::isRtl())
        <link rel="stylesheet" href="{{Ecommerce::theme()->asset('style/bootstrap-rtl.min.css')}}"/>
    @endif
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css"/>
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset('style/main.css?v=1.0')}}"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.css"/>
    <title>@yield('page_title',"Home")@if(Ecommerce::settings()->get("ecommerce-site-name"))
            - {{Ecommerce::settings()->get("ecommerce-site-name")}}
        @endif</title>
    @stack("styles")
    <style>
        .primary_color {
            color: #f47a3c !important;
        }

        .secondary_color {
            color: #04b7bb !important;
        }

        .bg_primary_color {
            background-color: #f47a3c !important;
        }

        .bg_secondary_color {
            background-color: #04b7bb !important;
        }
    </style>
    @routes
    <script>var _token = '{{csrf_token()}}'</script>
</head>
<body>
{{--<div class="progress_preload">--}}
{{--    <div class="progress" id="progress_div">--}}
{{--        <div class="bar" id="bar1"></div>--}}
{{--    </div>--}}
{{--    <input type="hidden" id="progress_width" value="0"/>--}}
{{--</div>--}}

<div class="moboverlay"></div>
<header>
    <div class="container">
        <div class="header">
            <div class="logo">
                <a href="{{route('home')}}" class="anc_logo">
                    <figure>
                        <img src="{{Ecommerce::theme()->asset('images/logo.png')}}" alt="logo" width="111" height="60"/>
                    </figure>
                </a>
            </div>
            <nav>
                <button class="btn_close_nave btn_gh">
                    <i class="las la-times"></i>
                </button>
                @php
                    //                    $settingPages = [
                    //                        'about'=>Ecommerce::theme()->settings()->get('page-about',0)??0,
                    //                        'care'=>Ecommerce::theme()->settings()->get('page-care',0)??0,
                    //                        'steam_wash'=>Ecommerce::theme()->settings()->get('page-steam-wash',0)??0
                    //                        ];
                $pages  = \Tasawk\Cms\Models\Page::where('status',1)->where('type','text')->listsTranslations("page_title")->pluck('page_title','id')
                @endphp
                <ul class="ul_nav ul_gh">
                    @foreach($pages as $id=>$page)
                        <li>
                            <a href="{{route('page.show',['page'=>$id])}}"
                               class="anc_nav anc_gh">{{$page}} </a>
                        </li>
                    @endforeach
                    <li>
                        <a
                            href="{{route('home',['#id-service-models'])}}"
                            class="anc_nav anc_gh click_position"
                            data-id="id-service-models"
                        >
                            @lang("Service models")
                        </a>
                    </li>
                    <li>
                        <a
                            href="{{route('home',['#id-faq'])}}"
                            class="anc_nav anc_gh click_position"
                            data-id="id-faq"
                        >
                            @lang("FAQ")

                        </a>
                    </li>
                    <li>
                        <a
                            href="{{route('home',['#id-contact-us'])}}"
                            class="anc_nav anc_gh click_position"
                            data-id="id-contact-us"
                        >@lang("Get in touch")</a
                        >
                    </li>
                    <li>
                        @php($convertLang = (app()->getLocale() == "en") ? "ar" : "en")
                        <a href="{{ LaravelLocalization::getLocalizedURL($convertLang, null, [], true) }}"
                           class="anc_nav anc_gh">
                            {{$convertLang == 'en'?'English':"العربية"}}
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="div-order-now_bars">

                <a
                    @php($order_now_url = \Illuminate\Support\Facades\Route::getCurrentRoute()->getName() == 'home'? '#':route('home',['#id-package']))
                    href='{{$order_now_url}}'
                    class="anc_gh order-now effects_ click_position cart-btn m-0"
                    data-id="id-package"
                >
                    {{Ecommerce::OrderNowButtonText()}}
                </a>
                <button class="btn_bars btn_gh"><i class="las la-bars"></i></button>
            </div>
        </div>
    </div>
</header>
