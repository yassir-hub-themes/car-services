<div class="content_finish">
    @foreach( app('cart')->getContent() as $item)
        <h3 class="m_P_gh title_finish_item">   {{$item->name}}</h3>
        @foreach($item->attributes['options'] as $attribute)
            <p class="m_P_gh">{{$attribute['name']}}:{{$attribute['value']}}</p>
        @endforeach
        <span class="price_finish"> <span class="cart_price">{{Ecommerce::formatPrice(app('cart')->getTotal())}}</span> @lang('R.S')</span>
    @endforeach
</div>
