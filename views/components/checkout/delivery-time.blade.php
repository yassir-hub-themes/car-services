<?php
$allowed_shipping_dates_types = settings()->group('ecommerce')->collect('allowed_shipping_dates_types', ["immediately", "as_soon_as_possible", "select_date_time"]);
?>
<div class="delivery-time-content "
     style="display: {{$allowed_shipping_dates_types->isEmpty() ? 'none' : 'block'}} ;margin-top: 100px;">
    {{html()->element('h2')->class('page-title primary_color')->html(__('Choose a date'))}}
    <div class="delivery-time">
        <input type="hidden" id="delivery_date" value="{{old('delivery_date',date('Y-m-d'))}}" name="delivery_date">
        <input type="hidden" id="delivery_time" value="{{old('delivery_time',date('H:i'))}}" name="delivery_time">
        {{html()->hidden('shift-id')}}
        @if($allowed_shipping_dates_types->contains('as_soon_as_possible'))
            <label class="delivery-item">
                {{html()->radio('delivery_type')->value('as_soon_as_possible')->checked(old('delivery_type') == 'as_soon_as_possible')}}
                <div class="delivery-item-text">
                    <div class="text">
                        <i class="las la-stopwatch"></i>
                    </div>
                </div>
                <span> @lang('As soon as possible')</span>
            </label>
        @endif
        @if($allowed_shipping_dates_types->contains('immediately'))
            <label class="delivery-item">
                {{html()->radio('delivery_type')->value('immediately')->checked(old('delivery_type') == 'immediately')}}
                <div class="delivery-item-text">
                    <div class="text">
                        <i class="las la-bolt"></i>
                    </div>
                </div>
                <span>@lang('Immediately')</span>
            </label>
        @endif
        @if($allowed_shipping_dates_types->contains('select_date_time') && !empty($shifts))
            <label class="delivery-item">
                <input type="radio" name="delivery_type" value="select_date_time"
                       @if(old('delivery_type') == 'select_date_time') checked @endif class="select-date">
                <div class="delivery-item-text">
                    <div class="text">
                        <i class="las la-clock"></i>
                    </div>
                </div>
                <span>  @lang('Select date')</span>
            </label>
        @endif
    </div>
    @if($errors->has('delivery_type')||$errors->has('delivery_date')||$errors->has('delivery_time'))
        <p class="text-center" style=" margin-top: 40px!important;">
        <span class="error-text-alert">
            @error('delivery_type'){{ $message }}@enderror
            @error('delivery_date'){{ $message }}@enderror
            @error('delivery_time'){{ $message }}@enderror
        </span>
        </p>
    @endif
</div>
@push('modals')
    <div id="city_delivery_times_modal">
        @include("theme::components.checkout.modals.delivery_date")
    </div>
@endpush


