<section class="content-section checkout-content order-finish">
    <div class="container">
        <h1 class="title-single">@lang('Checkout')</h1>
        <h3 class="title-alert">
            @lang('Add the required data') (<span>*</span>) @lang('To complete the request')
        </h3>
        <form action="{{ route('checkout.store') }}" method="POST" id="checkout-store" autocomplete="off">
            @csrf
            @if (!Ecommerce::auth()->check() && $registration_enabled )
                @include('theme::components.checkout.register')
            @endif
            @if(!$registration_enabled && !Ecommerce::auth()->check())
                <p class="page-title">
                    @lang('Please')
                    <a href="{{route('login.form')}}">@lang('Log in')</a> /
                    <a href="{{route('register')}}">@lang('Register')</a>
                    @lang('To complete your order')</p>
            @endif
            @include("theme::components.checkout.map")
            @include('theme::components.checkout.delivery-time',['opening_days'=>$opening_days])
            @include('theme::components.checkout.payment-methods')
            @include('theme::components.checkout.brief')
            @include('theme::checkout.cart.discounts-inputs')
            <a class="cart-btn effects_" style="margin: 10px auto;">
                @lang("Confirm order")
            </a>
        </form>
    </div>
</section>
