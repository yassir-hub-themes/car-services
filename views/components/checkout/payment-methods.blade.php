<?php
$payments = settings()->group("ecommerce")->json("allowed_payment_methods");
$paymentProviders = app('payment')->enabledProviders();
if (empty($paymentProviders)) {
    return;
}
$myfatoorahProvider = (new Tasawk\Payments\Lib\Payment\Providers\MyFatoorahProvider);
$anotherProviders = [
    [
        'id' => $myfatoorahProvider->id(),
        'key' => 'apple_pay',
        'title' => __('Apple Pay'),
        'logo' => Ecommerce::theme()->asset("images/pay/pay3.png"),
    ],
    [
        'id' => $myfatoorahProvider->id(),
        'key' => 'stc_pay',
        'title' => __('stc Pay'),
        'logo' => Ecommerce::theme()->asset("images/pay/pay4.png")
    ],
    [
        'id' => $myfatoorahProvider->id(),
        'key' => 'madaa',
        'title' => __('Madaa'),
        'logo' => Ecommerce::theme()->asset("images/pay/pay5.png")
    ],
];
?>
<div class="payment-methods-content" style="    margin-top: 40px; margin-bottom: 50px;">
    <h2 class="page-title">
        @lang('Payment method')
    </h2>
    <div class="payment-methods">
        @foreach ($paymentProviders as $provider)
            <label class="payment-method" data-toggle="tooltip" data-placement="top"
                   title="{{$provider->title()}}">
                <input type="radio" name="payment_method" @if(old('payment_method') == $provider->id()  ) checked
                       @endif value="{{ $provider->id() }}">
                <div class="payment-img loading-img">
                    <img class="lazy-img"
                         alt="{{$provider->title()}}"
                         src="{{ $provider->logo() }}">
                </div>
                <span>@lang($provider->title())</span>
            </label>
        @endforeach

        @if($myfatoorahProvider->isEnabled())
            @foreach($anotherProviders as $provider)
                <label class="payment-method" data-toggle="tooltip" data-placement="top" title="{{$provider['title']}}">
                    <input type="radio" name="payment_method" class="myfatoorah_payment_methods"
                           data-key="{{$provider['key']}}"
                           @if(old('payment_method') == $provider['id'] && old('myfatoorah_payment') == $provider['key'] ) checked
                           @endif value="{{ $provider['id'] }}"/>
                    <div class="payment-img loading-img">
                        <img class="lazy-img"
                             alt="{{$provider['title']}}"
                             src="{{ $provider['logo'] }}">
                    </div>
                    <span>@lang($provider['title'])</span>
                </label>
            @endforeach

        @endif
        <input type="hidden" name="myfatoorah_payment" class="myfatoorah_payment_method">
    </div>
    @error('payment_method')
    <p class="text-center" style="margin-top: 40px!important;">
        <span class="error-text-alert">{{ $message }}</span>
    </p>
    @enderror
</div>

@push("scripts")
    <script>
        $(".myfatoorah_payment_methods").on("click", function () {
            if ($(this).is(":checked")) {
                $(".myfatoorah_payment_method").val($(this).data('key'))
            }
        });
    </script>
@endpush
