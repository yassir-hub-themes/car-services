@php($services = \Tasawk\Cms\Model\Service::find( settings()->group('theme_car_services')->json('services')))
@if($services->count())
    <section class="service-models-section" id="id-service-models">
        <div class="title_div">
            <h2 class="m_P_gh">@lang("Service models")</h2>
        </div>
        <div class="container">
            <div class="service-models">
                <div class="swiper mySwiper">
                    <div class="swiper-wrapper">
                        @foreach($services as $service)
                            @php($service_image = \Illuminate\Support\Facades\File::exists($service->getFirstMedia()?->getPath())
                                                ? upload_storage_url($service->getFirstMedia())
                                                : Ecommerce::theme()->asset('images/services/01.jpg'))
                            <div class="swiper-slide">
                                <div class="service_item">
                                    <a href="{{$service_image}}"
                                       data-fancybox="gallery">
                                        <img src="{{$service_image}}" alt="{{$service->title}}"/>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next">
                    <i class="la la-arrow-right"></i>
                </div>
                <div class="swiper-button-prev">
                    <i class="la la-arrow-left"></i>
                </div>
            </div>
        </div>
    </section>
@endif
