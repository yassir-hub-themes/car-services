@php($faqs = \Tasawk\Cms\Models\Page::find( Ecommerce::theme()->settings()->get('page-faqs',0)))

@if($faqs && $faqs->count())
    <section class="faq_section" id="id-faq">
        <div class="container">
            <div class="faq">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="content_faq">
                            <div class="title_div">
                                <h4 class="m_P_gh">@lang("FAQ")</h4>
                                <h2 class="m_P_gh">
                                    @lang("To offer better service We answered the most common questions")
                                </h2>
                            </div>
                            <p class="p_content">
                                @lang('If you do not find an answer to your query, do not hesitate to contact us directly')
                            </p>
                            <a
                                href="#"
                                class="btn_order_now effects_2 anc_gh click_position"
                                data-id="id-contact-us"
                            >@lang("Contact us")
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        @if(isset($faqs->content['questions']))
                            <div class="gruop-collase">
                                @foreach($faqs->content['questions'] as $question)
                                    <div class="collapse_parant">
                                        <div class="number_c color_toggle">1</div>
                                        <div class="group_collapse">
                                            <button class="btn_gh btn_collapse_ color_toggle inactive">
                                                {{$question}}
                                                <div class="icon-wrapper">
                                                    <div class="bar backg_toggle bar--vertical"></div>
                                                    <div class="bar backg_toggle bar--horizontal"></div>
                                                </div>
                                            </button>
                                            <div class="toggle_collapse">
                                                {{\Illuminate\Support\Arr::get($faqs->content['answer'],$loop->index)}}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
