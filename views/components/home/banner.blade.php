@php($sliders = \Tasawk\Sliders\Model\Slider::where('status',1)->whereIn("id", settings()->group('theme_car_services')->json('sliders'))->get())
@if($sliders && $sliders->count())
<main class="section-banner">
    <div class="banner">
        <div class="swiper mySwiper">
            <div class="swiper-wrapper">
                @foreach($sliders as $slider)
                    <div class="swiper-slide">
                        <a href="{{$slider->link}}" class="anc_banner anc_gh">
                            <figure>
                                <img src="{{upload_storage_url($slider->image)}}" alt="img banner"/>
                            </figure>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next">
            <i class="la la-arrow-right"></i>
        </div>
        <div class="swiper-button-prev">
            <i class="la la-arrow-left"></i>
        </div>
    </div>
</main>
@endif
