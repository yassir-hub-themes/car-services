@php
    $latestProducts = app('items')->latestProducts();
@endphp

@if($latestProducts->count())
    <section class="packages_section" id="id-package">
        <div class="title_div">
            <h4 class="m_P_gh">@lang("Services we provide professionally")</h4>
            <h2 class="m_P_gh text-capitalize">@lang("packages")</h2>
        </div>
        <div class="container">
            <div class="packages">
                <div class="div-tab-toggle slider1">
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            @foreach($latestProducts as $product)
                                <div class="swiper-slide showProductDetails"
                                     data-item-id="{{$product->id}}">
                                    <div class="item">
                                        <a href="{{upload_storage_url($product->getImage())}}"
                                           class="anc_package anc_gh">
                                            <figure>
                                                <img
                                                    src="{{upload_storage_url($product->getImage())}}"
                                                    alt="{{$product->title}}"
                                                    data-fancybox="one_time_{{$product->id}}"
                                                    style="width: 263px;height: 263px"
                                                />
                                            </figure>
                                            <h3>{{$product->title}}</h3>
                                        </a>
                                        <div class="btn_order_now effects_2 btn_gh "
                                        >
                                            {{Ecommerce::OrderNowButtonText()}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-next">
                        <i class="la la-arrow-right"></i>
                    </div>
                    <div class="swiper-button-prev">
                        <i class="la la-arrow-left"></i>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
@push("scripts")
    <script>
        $(function () {
            $(".showProductDetails").on("click", function (e) {
                e.preventDefault();
                const ID = $(this).data('item-id');
                let submitButton = $(this).find('.btn_order_now')
                $.ajax({
                    url: route('products.pop_up_details', ID),
                    beforeSend: function () {
                        submitButton.append(' <i class="las la-spinner la-lg"></i>')
                    },
                    success: function (data) {
                        submitButton.html("{{Ecommerce::OrderNowButtonText()}}")
                        $('#modals-container').html(data);
                        $(".order_popup , .moboverlay").slideToggle("500");
                        $("body").addClass("over_");
                        $('form#orderNow .select2').select2();
                        $('.selected-item-option,.radio-item-option:checked').trigger('change');

                    }
                })
            });
        });
    </script>
@endpush
