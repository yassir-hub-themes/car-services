@php($features = \Tasawk\Cms\Model\Feature::find( settings()->group('theme_car_services')->json('features')))
@if($features->count())
    <section class="why-choose-section">
        <div class="title_div">
            <h2 class="m_P_gh">@lang("Benefits of our services")</h2>
        </div>
        <div class="container">
            <div class="why-choose">
                @foreach($features as $feature)
                    @php($feature_image = \Illuminate\Support\Facades\File::exists($feature->getFirstMedia()?->getPath())
                                        ? upload_storage_url($feature->getFirstMedia())
                                        : Ecommerce::theme()->asset('images/icons/01.png'))

                    <div class="item-why">
                        <div class="icon-div">
                            <figure><img src="{{$feature_image}}" alt="{{$feature->title}}"/></figure>
                        </div>
                        <span class="name_service">{{$feature->title}}</span>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
@endif
