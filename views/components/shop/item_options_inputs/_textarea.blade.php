<div class="form-group">
    <label>
        {{$option->name}}
        @if($option->pivot->required)
            <span class="text-danger">*</span>
        @endif
    </label>
    <textarea class="form-control" name="option[option_{{$option->id}}]">{{old("option_{$option->id}",$option->pivot->value)}}</textarea>
    @error("option_$option->id")
    <p class="text-danger">{{$message}}</p>
    @enderror

</div>
