@php
    $item_options_pivot = $option->getValuesByItem($item->id)->get()->pluck("pivot");
    $options_value = $option->values()->orderBy("sort_order","asc")->get();
@endphp
<div class="form-group">
    <label>
        {{$option->name}}
        @if($option->pivot->required)
            <span class="text-danger">*</span>
        @endif
    </label>
    @foreach($options_value as $value)
        @php($item_option_value =$item_options_pivot->where("option_value_id",$value->id)->first())
        @php( $final_price = Ecommerce::price($item_option_value["price"] ?? 0)->withTaxes($item->taxs->sum("cost"))->final())
        @continue(!Arr::has($item_option_value,"price"))

        <div class="form-check mb-4">
            <input data-id="{{$option->id}}" class="form-check-input radio-item-option "
                   id="option[option_{{$value->id}}]"
                   data-price="{{Ecommerce::formatPrice(Ecommerce::calcCurrencyRatio($final_price))}}"
                   type="radio"
                   @if($loop->first)
                       checked
                   @endif
                   name="option[option_{{$option->id}}]"
                   value="{{$value->id}}"
            >
            <label for="option[option_{{$value->id}}]" class="form-check-label mx-3">
                {{$value->name}}

                ({{Ecommerce::formatPrice(Ecommerce::calcCurrencyRatio($final_price))}}  {{Ecommerce::currentSymbol()}})
            </label>
        </div>
    @endforeach

    @error("option_$option->id")
    <p class="text-danger">{{$message}}</p>
    @enderror
</div>
