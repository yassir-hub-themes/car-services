<div class="form-group">
    <label>
        {{$option->name}}
        @if($option->pivot->required)
            <span class="text-danger">*</span>
        @endif
    </label>
    <input class="form-control" type="{{$option->type}}" name="option[option_{{$option->id}}]" value="{{old("option_{$option->id}",$option->pivot->value)}}"/>
    @error("option_$option->id")
    <p class="text-danger">{{$message}}</p>
    @enderror

</div>
