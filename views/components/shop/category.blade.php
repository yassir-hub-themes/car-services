@php($active = in_array($category->id,[$selected_category->id,$selected_category->parent]))

<a href="{{route('shop.category.products',$category->id)}}"
   class="list-group-item @if($active) active @endif">
    @for($i =0; $i<$level; $i++)-@endfor
    {{$category->title}} ({{$category->items()->enabled()->count()}})</a>
@if($category->children->count())
    @php($level++)
    @foreach($category->children as $child)
        @include("theme::components.shop.category",['category'=>$child])
    @endforeach
@endif

