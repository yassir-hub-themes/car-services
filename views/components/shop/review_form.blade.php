@if(Ecommerce::auth()->check())
    {{html()->form('post')->action(route('comments.store'))->class('form-horizontal')->id('form-review')->open()}}
    <h2>@lang('Write review')</h2>
    <input type="hidden" name="commentable_type" value="{{ get_class($item) }}"/>
    <input type="hidden" name="commentable_id" value="{{ $item->getKey() }}"/>
    <div class="comment-group required">
        <div class="col-sm-12">
            {{html()->label(trans("Comment"))->for('input-review')->class('control-label')}}
            {{html()->textarea("message")->id("input-review")->class('form-control')->value(old('message'))->rows(8)->style(['resize'=>'none','padding'=>'15px'])}}
            @error('message')
            <div class="invalid-feedback text-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="comment-group required">
        <div class="col-sm-12">
            <div class="rate-cont">
                <label class="control-label">@lang("Rate"):</label>
                <div class="rate">
                    <input type="radio" id="comment_star_5" name="rating" value="5"/>
                    <label for="comment_star_5" title="text">5</label>
                    <input type="radio" id="comment_star_4" name="rating" value="4"/>
                    <label for="comment_star_4" title="text">4</label>
                    <input type="radio" id="comment_star_3" name="rating" value="3"/>
                    <label for="comment_star_3" title="text">3</label>
                    <input type="radio" id="comment_star_2" name="rating" value="2"/>
                    <label for="comment_star_2" title="text">2</label>
                    <input type="radio" id="comment_star_1" name="rating" value="1"/>
                    <label for="comment_star_1" title="text">1</label>
                </div>
            </div>
            @error('rating')
            <div class="invalid-feedback text-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="buttons clearfix">
        <div class="pull-right">
            <button type="submit" id="button-review"
                    class="btn btn-primary">@lang('Continue')</button>
        </div>
    </div>
    {{html()->form()->close()}}
@else
    <p> @lang('Please') <a class="login-link-page"
                           href="{{route('login.form')}}">@lang('Log in')</a> @lang('to write a review')</p>
@endif
