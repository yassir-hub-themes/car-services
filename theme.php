<?php
/**
 *sitemap
 *
 **/

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Tasawk\AppSettings\Setting\AppSettings;
use Tasawk\Banners\Model\Banner;
use Tasawk\Cms\Model\Feature;
use Tasawk\Cms\Model\Service;
use Tasawk\Cms\Models\Page;
use Tasawk\Sliders\Model\Slider;

Ecommerce::registerViews([
    'cart-total-summation' => 'theme::checkout.cart.total-summation'
]);


Payment::registerViews([
    'cancel' => 'theme::checkout.payment.cancel',
    'success' => 'theme::checkout.payment.success',
    'error' => 'theme::checkout.payment.error'
]);

$pages = [
    'About',
    'Steam wash',
    'Care',
    'FAQs',
];
$pageInputs = [];
foreach ($pages as $page) {
    $pageInputs[] = [
        'type' => 'select',
        'name' => Str::slug('page-' . $page),
        'label' => $page,
        'placeholder' => "Select",
        'rules' => 'nullable|numeric',
        'options' => function () {
            return Page::listsTranslations("page_title")->pluck('page_title', 'id');
        }
    ];
}

$data = [
    'title' => 'Car services Theme',
    'icon' => 'icon-browser',
    'group' => 'theme_car_services',
    'descriptions' => 'Control Ecommerce theme',
    'permission' => "manage_settings.landing_page",
    'sections' => [
        'general' => [
            'columns' => 2,
            'title' => 'General',
            'icon' => 'icon-cog2',
            'inputs' => [
                [
                    'name' => 'html_block',
                    'type' => 'html',
                    "content" => view("theme::settings.order_now_text"),
                    "html_inputs" => [
                        [
                            'name' => 'order_now_button_ar_text',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                        [
                            'name' => 'order_now_button_en_text',
                            'type' => 'text',
                            'rules' => 'nullable',
                        ],
                    ],
                ],
                [
                    'type' => 'select',
                    'name' => 'sliders',
                    'label' => 'Sliders',
                    'placeholder' => __("Select"),
                    'data_type' => 'array',
                    'multiple' => true,
                    'options' => function () {
                        return Slider::listsTranslations("title")->pluck("title", "id");
                    }
                ],
                [
                    'type' => 'select',
                    'name' => 'features',
                    'label' => 'Features',
                    'data_type' => 'array',
                    'placeholder' => __("Select"),
                    'options' => function () {
                        return Feature::listsTranslations("title")->pluck("title", "id");
                    },
                    'multiple' => true
                ],
                [
                    'type' => 'select',
                    'name' => 'services',
                    'label' => 'Services',
                    'data_type' => 'array',
                    'placeholder' => __("Select"),
                    'options' => function () {
                        return Service::listsTranslations("title")->pluck("title", "id");
                    },
                    'multiple' => true
                ],
            ]
        ],
        'actions' => [
            'columns' => 2,
            'title' => 'Actions',
            'icon' => 'icon-cog2',
            'inputs' => [
                [
                    'name' => 'incoming_order_due_date_depend_on_zones_capacity',
                    'label' => 'Incoming orders due dates will depend on zones capacity,',
                    'type' => 'boolean',
                    'placeholder' => 'write diameter with meter',
                    'hint' => "Zones capacity determine based on total worker tasks capacity divided on zone shifts count.",
                    'value' => false,

                ],
                [
                    'type' => 'boolean',
                    'label' => 'Assign incoming orders to workers automatically based on worker location',
                    'name' => 'assign_incoming_orders_to_workers_automatically_based_on_location_enabled',
                    'value' => false,
                ],
                [
                    'name' => 'auto_assigned_geofencing_diameter',
                    'label' => 'Assignment diameter',
                    'type' => 'number',
                    'placeholder' => 'write diameter with meter',
                    'hint' => 'if the input left blank the system consider diameter as 5000 meter as default.',
                    'min' => 0,
                    'hide_when' => [
                        [
                            'selector' => '#geo_fencing_type',
                            'value' => 'area'
                        ]
                    ],

                ],

            ]
        ],
        'pages' => [
            'columns' => 2,
            'title' => 'Pages',
            'icon' => 'icon-pen',
            'inputs' => $pageInputs
        ]

    ],
];

AppSettings::addPage('theme_car_services', $data);


